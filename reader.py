import os
import sys
import csv

src = sys.argv[1]
dst = sys.argv[2]
zmiany = sys.argv[3:]

def zmien_csv():
    # katqalog1/katalog11/katalog111/mlb_players.csv
    wyzszy_katalog, nazwa_pliku = os.path.split(src)

    if not os.path.isfile(src):
        print(f"Podany plik {nazwa_pliku} nie istnieje")
        print(f"Pliki w katalogu {wyzszy_katalog}:")
        print(os.listdir(wyzszy_katalog))
        return

    with open(src, "r") as plik:
        reader = csv.reader(plik, skipinitialspace=True)
        zawartosc_pliku = []
        for wiersz in reader:
            zawartosc_pliku.append(wiersz)

    for zmiana in zmiany:
        z = zmiana.split(',')

        liczba_wierszy = len(zawartosc_pliku)
        liczba_kolumn = len(zawartosc_pliku[0])

        if liczba_wierszy > int(z[0]) and liczba_kolumn > int(z[1]):
            zawartosc_pliku[int(z[0])][int(z[1])] = z[2]

    sciezka_nowego_pliku = os.path.join(dst, nazwa_pliku)
    if not os.path.isdir(dst):
        os.makedirs(dst)

    with open(sciezka_nowego_pliku, "w") as plik_zapisu:
        writer = csv.writer(plik_zapisu)
        for wiersz in zawartosc_pliku:
            writer.writerow(wiersz)


zmien_csv()
